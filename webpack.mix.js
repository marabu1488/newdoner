const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
    .extract(['vue'])
    .sass('resources/sass/app.scss', 'public/css')
    .styles([
        'resources/css/account.css',
        'resources/css/admin_panel.css',
        'resources/css/admin_panel_orders.css',
        'resources/css/amimation.css',
        'resources/css/buscket.css',
        'resources/css/header_footer.css',
        'resources/css/layot.css',
        'resources/css/lefter.css',
        'resources/css/main.css',
        'resources/css/popups.css',
        'resources/css/radio.css',
        'resources/css/slick.css',
        'resources/css/stylesheet.css',
    ], 'public/css/all.css');
