<?php

namespace App\Http\Controllers;

use App\Models\Additive;
use Illuminate\Http\Request;

class AdditiveController extends Controller
{
    public function index()
    {
        $additive = Additive::all();
        return view('', compact('additive'));
    }

    public function create(Request $request)
    {
        return Additive::create([
            'name' => $request->post('name'),
            'image' => $request->post('image'),
        ]);
    }

    public function update(Additive $additive, Request $request)
    {
        $additive->update($request->all());

        return $additive;
    }

    public function delete(Additive $additive, Request $request)
    {
        $additive->delete();
    }
}
