<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function index()
    {
        $categories = Category::all();
        return view('admin.categories', compact('categories'));
    }

    public function create(Request $request)
    {
        return Category::create([
            'name' => $request->post('name')
        ]);
    }

    public function update(Category $category, Request $request)
    {
        $category->update($request->all());

        return $category;
    }

    public function delete(Category $category, Request $request)
    {
        $category->delete();
    }
}
