<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Product;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    public function index()
    {
        $products = Product::all();
//        dd(json_encode($products), JSON_UNESCAPED_UNICODE);
        return view('admin.home', compact('products'));
    }
}
