<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = "products";

    public function ingredients()
    {
        return $this->belongsToMany(Ingredient::class);
    }

    public function orders()
    {
        return $this->belongsToMany(Order::class);
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function additives()
    {
        return $this->belongsToMany(Additive::class);
    }
}
