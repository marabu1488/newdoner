<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Additive extends Model
{
    protected $table = "additives";

    protected $fillable = [
        'name', 'image'
    ];

    public function products()
    {
        return $this->belongsToMany(Product::class);
    }
}
