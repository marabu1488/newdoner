<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->id();
            $table->string('name')->unique();
            $table->string('desc')->nullable();
            $table->string('image');
            $table->integer('price')->unsigned();
            $table->integer('proteins')->unsigned()->nullable();
            $table->integer('fats')->unsigned()->nullable();
            $table->integer('energy')->unsigned()->nullable();
            $table->integer('carbo')->unsigned()->nullable();
            $table->boolean('top')->default(0);
            $table->boolean('visibility')->default(1);
            $table->foreignId('category_id');

            $table->foreign('category_id')->references('id')->on('categories');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
