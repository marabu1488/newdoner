<?php

use App\Models\Category;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $doners = new Category();
        $doners->name = 'Донеры';
        $doners->save();

        $burgers = new Category();
        $burgers->name = 'Бергеры';
        $burgers->save();

        $kebabs = new Category();
        $kebabs->name = 'Шашалыки';
        $kebabs->save();

        $snacks = new Category();
        $snacks->name = 'Снеки';
        $snacks->save();

        $salads = new Category();
        $salads->name = 'Салаты';
        $salads->save();

        $soups = new Category();
        $soups->name = 'Супы';
        $soups->save();

        $drinks = new Category();
        $drinks->name = 'Напитки';
        $drinks->save();

        $sauces = new Category();
        $sauces->name = 'Соусы';
        $sauces->save();

        $supplements = new Category();
        $supplements->name = 'Добавки';
        $supplements->save();
    }
}
