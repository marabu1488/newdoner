<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="stylesheet" href="{{ URL::asset('css/all.css') }}">
    <meta id="token" name="csrf-token" content="{{ csrf_token() }}">
    <link href="https://fonts.googleapis.com/css2?family=Russo+One&display=swap" rel="stylesheet">
    <title>Панель управления</title>
</head>

<body>
<div id="app">
    <main>
        <!-- Верстка левой панели управления -->
        <div class="work_panel">
            <div class="work_panel_photo">
                <img src="../img/Group 348 (1).svg" alt="" class="user_avatar">
            </div>
            <p class="user_name">{{Auth::user()->name}}</p>

            <div class="work_panel__inner">
                <a class="products admin_link active">Продукты</a>

                <a class="statistics admin_link">Статистика</a>

                <a class="orders admin_link">Заявки</a>
            </div>
        </div>
        <!-- Верстка левой панели управления -->


        <!-- Верстка  списка категорий и товаров в админ панели -->
        <view-products-component></view-products-component>
        <!-- Верстка  списка категорий и товаров в админ панели -->


        {{--        <!-- Верстка попапа с добавлением товаров на выбор -->--}}
        <div class="admin__container_">
            <create-product-component></create-product-component>
        </div>
        {{--                <!-- Верстка попапа с добавлением товаров на выбор -->--}}


                        <!-- Вестка попап с редактированием товара на выбор  -->

                        <div class="admin__container_">
                          <redacting-product-component></redacting-product-component>
                        </div>

        {{--        <!-- Вестка попап с редактированием товара на выбор  -->--}}


        {{--        <!-- Верстка попапа для уточнения удаления товара -->--}}


        {{--        <div class="admin__container_">--}}
        {{--            <div class="are_you_serious_popup" id="are_you_serious" :class="{active : activation}">--}}
        {{--                <div class="are_you_serious_modal_win">--}}
        {{--                    <h3 class="delete_popup_title">--}}
        {{--                        Вы действительно хотите удалить данную позицию?--}}
        {{--                    </h3>--}}
        {{--                    <div class="buttons_delete_block">--}}
        {{--                        <button class="delete_item working_btn" @click='totalDelete'>Да</button>--}}
        {{--                        <button class="non_delete_item working_btn" @click='falseDelete'>Нет</button>--}}
        {{--                    </div>--}}
        {{--                </div>--}}
        {{--            </div>--}}
        {{--        </div>--}}


        {{--        <!-- Верстка попапа для уточнения удаления товара -->--}}

    </main>
</div>
<script src="{{asset('/js/manifest.js')}}"></script>
<script src="{{asset('/js/vendor.js')}}"></script>
<script src="{{ asset('js/app.js') }}" defer></script>
<script src="https://code.jquery.com/jquery-3.5.1.js"
        integrity="sha256-QWo7LDvxbWT2tbbQ97B53yJnYU3WhH/C8ycbRAkjPDc=" crossorigin="anonymous"></script>
<script src="https://kit.fontawesome.com/91843ad480.js" crossorigin="anonymous"></script>
</body>

</html>
