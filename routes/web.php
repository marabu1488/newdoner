<?php

use App\Http\Controllers\Admin\AdminController;
use App\Http\Controllers\Auth\AdminLoginController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'IndexController@index')->name('main');

Auth::routes();
Route::get('/admin', [AdminLoginController::class, 'showLoginForm']);
Route::post('/adminLogin', [AdminLoginController::class, 'login'])->name("loginAdmin");
Route::get('/getProducts','ProductController@getProducts')->name('getProducts');

Route::group(['middleware' => ['admin', 'adminAuth']], function () {
    Route::get('/dashboard', [AdminController::class, 'index'])->name('homeAdmin');
    //-------------------------------------------------------------------------------------------------
    Route::get('/categories', 'CategoryController@index')->name('categories');
    //--------------------------------------------------------------------------------------------------
    
    Route::post('/productAdd', 'ProductController@create')->name('productAdd');
    Route::post('/productUpdate', 'ProductController@update')->name('productUpdate');
    Route::post('/productDelete', 'ProductController@delete')->name('productDelete');
    Route::post('/productVisibility', 'ProductController@visibility')->name('visibility');
    //--------------------------------------------------------------------------------------------------
    Route::get('/additives', 'AdditiveController@index')->name('additives');
    Route::post('/additiveAdd', 'AdditiveController@create')->name('additiveAdd');
    Route::post('/additiveUpdate', 'AdditiveController@update')->name('additiveUpdate');
    Route::post('/additiveDelete', 'AdditiveController@delete')->name('additiveDelete');
});


Route::get('/home', 'HomeController@index')->name('home');
